<?php
/**
 * Created by PhpStorm.
 * User: Shan
 * Date: 12/16/2016
 * Time: 11:41 AM
 */

$config  =
    [

        'add_article_rules' => [
                                    [
                                        'field' => 'title',
                                        'label' => 'Title',
                                        'rules' => 'required|alpha'
                                    ],
                                    [
                                        'field' => 'body',
                                        'label' => 'Article Body',
                                        'rules' => 'required'
                                    ]
                            ]

    ]
?>