<?php

class Admin extends MY_Controller
{
	
    public function __construct()
    {
        parent:: __construct();
        if(!$this->session->userdata("user_id"))
            return redirect("login");
        $this->load->helper('form');
        $this->load->model('articlemodel');
    }
    public function dashboard()
    {


        $data['list'] = $this->articlemodel->articles_list();

        $this->load->view("admin/dashboard",$data);
    }

    public function add_article()
    {
        $this->load->view("Admin/add_article");
    }

    public function submit_article()
    {
        $this->load->library("form_validation");
        if($this->form_validation->run('add_article_rules'))
        {
            $article_data['uid'] =  $this->session->userdata('user_id');
            $article_data['title'] =    $this->input->post("title");
            $article_data['body'] =     $this->input->post("body");
            $result = $this->articlemodel->insert_article($article_data);
            if($result)
            {
                $this->session->set_flashdata('submitted', 'Article Successfully Submitted');
                return redirect('admin/add_article');
            }
            else
            {
                $this->session->set_flashdata('failed', 'Article cannot be Submitted Try Again');
            }

        }
        else
            return $this->load->view('admin/add_article');

    }
    public function edit_article()
    {
        $article_id =  $this->input->post('edit_id');
        $result = $this->articlemodel->fetch_article($article_id);
        $this->load->view('admin/update_article',['article'=>$result]);
    }

    public function update_article()
    {
        $this->load->library("form_validation");

        if($this->form_validation->run('add_article_rules'))
        {
            $article_data['uid'] =  $this->session->userdata('user_id');
            $article_data['title'] =    $this->input->post("title");
            $article_data['body'] =     $this->input->post("body");
            $article_id = $this->input->post("article_id");


            $result = $this->articlemodel->update_article($article_data,$article_id);
            if($result)
            {
                $this->session->set_flashdata('submitted', 'Article Successfully Submitted');
                return redirect('admin/dashboard');
            }
            else
            {
                $this->session->set_flashdata('failed', 'Article cannot be Submitted Try Again');
            }

        }
        else
            return $this->load->view('admin/edit_article');
    }


    public function delete_article()
    {
        $article_id =  $this->input->post('delete_id');
        $result = $this->articlemodel->delete_article($article_id);
        if($result)
        {
            return redirect('admin/dashboard');
        }

    }
}
