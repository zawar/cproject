<?php

Class Login extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('LoginModel');
	}
	public function index()
	{
        if($this->session->userdata("user_id"))
            return redirect("Admin/dashboard");
        else
		    $this->load->view("Public/admin_login.php");
	}
	
	public function admin_login()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username','Username','required|alpha');
		$this->form_validation->set_rules('password','Password','required');
		
		if($this->form_validation->run())
		{
			$Username = $this->input->post('username');
			$Password = $this->input->post('password');
			$id =  $this->LoginModel->login_validate($Username,$Password);
            if($id)
            {
                $this->session->set_userdata("user_id",$id);
                return redirect("admin/dashboard");
            }
            else {

                $this->session->set_flashdata('login_failed', 'Wrong Username/Password');
                return redirect("login");
            }

			
		}
		else
		{
			$this->load->view('Public/admin_login.php');
		}
	}

    public function admin_logout()
    {
        $this->session->unset_userdata('user_id');
        if(!$this->session->userdata('user_id'))
        {
            redirect('login');
        }
    }
}

?>