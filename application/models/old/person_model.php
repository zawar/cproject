<?php 
   class person_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
		 
		 $this->load->database();
		$this->load->database('dummy');
		 
      } 
	  
	  public function getAllUsers()
	  {
		  $query = $this->db->get('dummy');
		  return $query->result();
	  }
	  
	  public function insert_person_db($data)
	  {
		  return $this->db->insert('dummy', $data);
	  }
   
     
   } 
?> 