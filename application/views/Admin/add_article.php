<?php include('admin_header.php'); ?>

    <div class="container">
        <?php
            $form_array = array('class'=>'form-horizontal','method'=>'post');
            echo form_open('Admin/submit_article',$form_array);
        ?>

        <fieldset>
            <legend>Add Article</legend>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Article Title</label>
                        <div class="col-lg-6">
                            <?php
                                $title_field_array = array('class'=>'form-control','name'=>'title','placeholder'=>'Title','value'=>set_value('title'));
                                echo form_input($title_field_array);
                            ?>
                        </div>
                        <div class="col-lg-4">
                            <?php echo form_error('title'); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Article Body</label>
                        <div class="col-lg-6">
                            <?php
                            $body_field_array = array('class'=>'form-control','name'=>'body','placeholder'=>'Body','value'=>set_value('body'));
                            echo form_textarea($body_field_array);
                            ?>
                        </div>
                        <div class="col-lg-4">
                            <?php echo form_error('body'); ?>
                        </div>
                    </div>

                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <?php
                        $submit_btn_array = array('name'=>'submit','value'=>'Add Article','class'=>'btn btn-primary');
                        $reset_btn_array = array('name'=>'reset','value'=>'Clear','class'=>'btn btn-default');
                        echo form_reset($reset_btn_array);
                        echo form_submit($submit_btn_array);
                    ?>
                </div>
            </div>

            <?php if($error = $this->session->flashdata("submitted")):?>
                <div class="alert alert-dismissible alert-success">
                    <strong><?php echo $error ?></strong>
                </div>
            <?php elseif($error = $this->session->flashdata("failed")): ?>
                <div class="alert alert-dismissible alert-danger">
                    <strong><?php echo $error ?></strong>
                </div>
            <?php endif; ?>
            </fieldset>
        </form>
    </div>

<?php include('admin_footer.php');?>