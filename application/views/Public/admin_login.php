<?php include('public_header.php'); ?>

    <div class="container">
        <?php
            $form_array = array('class'=>'form-horizontal','method'=>'post');
            echo form_open('login/admin_login',$form_array);
        ?>

        <fieldset>
            <legend>Legend</legend>

        <?php if($error = $this->session->flashdata("login_failed")):?>
            <div class="alert alert-dismissible alert-warning">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <p><?php echo $error?></p>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label for="inputEmail" class="col-lg-2 control-label">Username</label>
              <div class="col-lg-6">
                <?php
                    $username_field_array = array('class'=>'form-control','name'=>'username','placeholder'=>'Username','value'=>set_value('username'));
                    echo form_input($username_field_array);
                ?>
              </div>
              <div class="col-lg-4">
                <?php echo form_error('username'); ?>
              </div>
        </div>
	
        <div class="form-group">
            <label for="inputPassword" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-6">
                <?php
                    $password_field_array = array('class'=>'form-control','name'=>'password','placeholder'=>'password');
                    echo form_password($password_field_array);
                ?>
            </div>
            <div class="col-lg-4">
                <?php echo form_error('password'); ?>
            </div>
        </div>
 
        <div class="form-group">
          <div class="col-lg-10 col-lg-offset-2">
            <?php
                $submit_btn_array = array('name'=>'submit','value'=>'Login','class'=>'btn btn-primary');
                $reset_btn_array = array('name'=>'reset','value'=>'Reset','class'=>'btn btn-default');
                echo form_reset($reset_btn_array);
                echo form_submit($submit_btn_array); ?>
          </div>
        </div>
        </fieldset>
</form>
</div>

 <?php include('public_footer.php');?>